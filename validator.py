from datetime import datetime
from rules import rules

def is_integer(value):
    try:
        value_converted_to_int = int(float(value))
        return True
    except ValueError:
        return False


def is_datetime(value):
    try:
        value_converted_to_datetime = datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
        return True
    except ValueError:
        return False

