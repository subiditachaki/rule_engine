import unittest
from rule_engine import RuleEngine, DataStream
from rules import rules
from collections import defaultdict


class TestRuleEngine(unittest.TestCase):
    def test_follow_rule_for_empty_input_and_no_rule(self):
        input_data = defaultdict(str)
        rules = defaultdict(list)
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertTrue(input_abides_by_rules)

    def test_follow_rule_for_single_integer_input_and_no_rule(self):
        input_data = {"signal": "ATL1", "value_type": "Integer", "value": "75.361"}
        rules = defaultdict(list)
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertTrue(input_abides_by_rules)

    def test_defy_rule_for_single_integer_input(self):
        input_data = {"signal": "ATL1", "value_type": "Integer", "value": "75.361"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertFalse(input_abides_by_rules)

    def test_follow_rule_for_single_integer_input(self):
        input_data = {"signal": "ATL1", "value_type": "Integer", "value": "678.361"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertTrue(input_abides_by_rules)

    def test_defy_rule_for_single_string_input(self):
        input_data = {"signal": "ATL2", "value_type": "String", "value": "LOW"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertFalse(input_abides_by_rules)

    def test_follow_rule_for_single_string_input(self):
        input_data = {"signal": "ATL2", "value_type": "String", "value": "HIGH"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertTrue(input_abides_by_rules)

    def test_defy_rule_for_single_datetime_input(self):
        input_data = {"signal": "ATL3", "value_type": "Datetime", "value": "2018-10-13 01:16:03"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertFalse(input_abides_by_rules)

    def test_follow_rule_for_single_datetime_input(self):
        input_data = {"signal": "ATL3", "value_type": "Datetime", "value": "2019-12-13 01:16:03"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertTrue(input_abides_by_rules)

    def test_pass_single_string_input_with_no_rules_available_for_signal(self):
        input_data = {"signal": "ATL99", "value_type": "Datetime", "value": "2019-12-13 01:16:03"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertTrue(input_abides_by_rules)

    def test_pass_single_string_input_with_no_rule_for_available_for_value_type_signal_combination(self):
        input_data = {"signal": "ATL2", "value_type": "Integer", "value": "123"}
        rules_to_apply = RuleEngine(rules)
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(input_data['signal'], input_data['value_type'],
                                                               input_data['value'])
        self.assertTrue(input_abides_by_rules)

    def test_given_single_integer_value_is_not_of_type_specified(self):
        input_data = {"signal": "ATL1", "value_type": "Integer", "value": "abc"}
        input_data_of_specified_type = DataStream.is_data_of_specified_type(input_data['value'],
                                                                            input_data['value_type'])
        self.assertFalse(input_data_of_specified_type)

    def test_given_single_integer_value_is_of_type_specified(self):
        input_data = {"signal": "ATL1", "value_type": "Integer", "value": "674.78"}
        input_data_of_specified_type = DataStream.is_data_of_specified_type(input_data['value'],
                                                                            input_data['value_type'])
        self.assertTrue(input_data_of_specified_type)

    def test_given_single_datetime_value_is_not_of_type_specified(self):
        input_data = {"signal": "ATL1", "value_type": "Datetime", "value": "abc"}
        input_data_of_specified_type = DataStream.is_data_of_specified_type(input_data['value'],
                                                                            input_data['value_type'])
        self.assertFalse(input_data_of_specified_type)

    def test_given_single_datetime_value_is_of_type_specified(self):
        input_data = {"signal": "ATL1", "value_type": "Datetime", "value": "2019-12-13 01:16:03"}
        input_data_of_specified_type = DataStream.is_data_of_specified_type(input_data['value'],
                                                                            input_data['value_type'])
        self.assertTrue(input_data_of_specified_type)


