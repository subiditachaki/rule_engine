import validator
from datetime import datetime


class RuleEngine:

    def __init__(self, rules):
        self.rules = rules

    def is_rule_abiding(self, signal, value_type, value):
        for rule in self.rules[(signal, value_type)]:
            return rule, RuleEngine.apply_rule(rule)(value)
        return None, True

    @staticmethod
    def apply_rule(rule_condition):
        return lambda value: eval(rule_condition)


class DataStream:

    def __init__(self, input_data):
        self.input_data = input_data

    def iterate_over_input_data(self):
        for row in self.input_data:
            if not DataStream.is_data_of_specified_type(row['value'], row['value_type']):
                continue
            yield row

    @staticmethod
    def is_data_of_specified_type(value, value_type):
        if value_type.lower() == 'string':
            return True
        func = getattr(validator, 'is_' + value_type.lower())
        if not func(value):
            return False
        return True



