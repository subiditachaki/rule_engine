import json
from datetime import datetime

from rule_engine import RuleEngine, DataStream
from rules import rules


def take_inputs():
    input_data = input()
    return input_data


def get_data_not_abiding_by_rules(rules_to_apply, data):
    data = DataStream(data)
    for row in data.iterate_over_input_data():
        rule, input_abides_by_rules = rules_to_apply.is_rule_abiding(row['signal'], row['value_type'], row['value'])
        if not input_abides_by_rules:
            print(f'The signal {row["signal"]} with value {row["value"]} and value type {row["value_type"]} '
                  f'does not abide by the rule {rule}')


if __name__ == '__main__':
    input_data = take_inputs()
    # input_data = open('raw_data.json', 'r').read().replace('\n', '')
    rules_to_apply = RuleEngine(rules)
    get_data_not_abiding_by_rules(rules_to_apply, json.loads(input_data))
