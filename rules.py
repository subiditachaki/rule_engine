from collections import defaultdict


rules = defaultdict(list)
# TODO do we need multiple?
rules[('ATL1', 'Integer')] = ['int(float(value)) > 240.00']
rules[('ATL2', 'String')] = ['value != "LOW"']
rules[('ATL3', 'Datetime')] = ['datetime.strptime(value, "%Y-%m-%d %H:%M:%S") >= datetime.now()']
